﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bridge : MonoBehaviour {

	public float length;

	[HideInInspector]
	public List<Vector3> begginingCor = new List<Vector3> ();

	[HideInInspector]
	public List<Vector3> endingCor = new List<Vector3> ();

	private List<SpriteRenderer> bridgeParts = new List<SpriteRenderer> ();

	// Use this for initialization
	void Start () {
		foreach (Transform child in transform) {
			if (child.tag == "bridge beginning") {
				foreach (Transform grandchild in child.transform) {
					if(grandchild.tag == "bridge beginning") {
						begginingCor.Add (grandchild.transform.position);
					}
					bridgeParts.Add(grandchild.gameObject.GetComponent<SpriteRenderer>());
				}
			} else if (child.tag == "bridge ending") {
				foreach (Transform grandchild in child.transform) {
					if(grandchild.tag == "bridge ending") {
						endingCor.Add (grandchild.transform.position);
					}
					bridgeParts.Add(grandchild.gameObject.GetComponent<SpriteRenderer>());
				}
			} else {
				foreach (Transform grandchild in child.transform) {
					bridgeParts.Add(grandchild.gameObject.GetComponent<SpriteRenderer>());
				}
			}
		}
	}


	void OnTriggerEnter2D(Collider2D  collider) 
	{

		if(collider.tag == "water")
		{
			collider.enabled = false;
		}
	}

	public void beginConstruction(float alpha) {
		foreach (SpriteRenderer render in bridgeParts) {
			Color oldColor = render.color;
			render.color = new Color(oldColor.r, oldColor.b, oldColor.g, alpha);
		}
		gameObject.SetActive (true);
	}

	public void buildBridge(){
		foreach (SpriteRenderer render in bridgeParts) {
			Color oldColor = render.color;
			render.color = new Color(oldColor.r, oldColor.b, oldColor.g, 1.0f);
		}
	}
	
}
