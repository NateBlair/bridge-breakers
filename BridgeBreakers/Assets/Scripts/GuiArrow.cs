﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiArrow : MonoBehaviour {

	public bool pressed;
	private Button button;

	void Start () {
		pressed = false;
		button = this.GetComponent<Button> ();
		button.interactable = false;
	}

	public void ChangeInteration(bool state) {
		button.interactable = state;
	}

	public void MouseEnter() {
		pressed = true;
	}

	public void MouseExit() {
		pressed = false;
	}

	public void MoveRight () {
		GameManager.instance.MovePlayerRight ();
	}

	public void MoveLeft () {
		GameManager.instance.MovePlayerLeft ();
	}
}
