﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatsManager : MonoBehaviour {

	private int topKills;
	private int topScore;
	private int topWaveReached;
	private int totalAttempts;
	private int lastScore;

	public Text scoreKeeper;

	void Start () {
		topScore = PlayerPrefs.GetInt ("highScore");
		topKills = PlayerPrefs.GetInt ("topKills");
		topWaveReached = PlayerPrefs.GetInt ("highWave");
		totalAttempts = PlayerPrefs.GetInt ("attempts");
		lastScore = PlayerPrefs.GetInt ("lastScore");

		scoreKeeper.text = "High Score: " + topScore + 
			"\n Most Kills: " + topKills + 
			"\n Highest Wave Reached: " + topWaveReached +
			"\n Total Attempts: " + totalAttempts + 
			"\n Last Games Score: " + lastScore;
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	public void loadLevel(string level) {
		SceneManager.LoadScene(level);
	}
}
