﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager instance = null;

	public static bool gameOver;

	public int secondBridgeSpawn;
	public int thirdBridgeSpawn;
	public int swordKnightSpawn;
	public int lightScountSpawn;
	public float deleyToSpawn;
	private float timeSinceSceneLoad;

	public Bridge leftBridge;
	public Bridge rightBridge;
	public Bridge centralBridge;
	private float bridgeFadeValue;

	public int spearmanMinSpawn;
	public int spearmanMaxSpawn;
	public int swordmanMinSpawn;
	public int swordmanMaxSpawn;
	public int lightScoutMinSpawn;
	public int lightScoutMaxSpawn;
	public long difficultyMultiplier;
	public float timeBetweenWavesSec;

	public Player player;
	private string playerPosition;
	public Enemy spearmanPrefab;
	public Enemy swordmanPrefab;
	public Enemy lightscoutPrefab;

	public float distanceFromBridge;

	private List<Enemy> activeEnemies = new List<Enemy> ();

	private List<GameObject> spawnLocations = new List<GameObject> ();
	private Vector3 playerLeftPosition;
	private Vector3 playerCentralPosition;
	private Vector3 playerRightPosition;

	private bool leftBridgeActive;
	private bool rightBridgeActive;
	public GuiArrow leftArrow;
	public GuiArrow rightArrow;

	private bool spawnedFirstWave;
	private int wave;
	private float lastSpawnTime;
	private float spawnPointTimer;

	public GameObject gameOverPanel;

	public Text scoreText;
	private int score;
	public Text killText;
	private int kills;
	public Text waveText;
	public GuiTextBlinker approachingWave;
	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != null) {
			Destroy(gameObject);
		}		
	}
	void Start () {
		//Initialize internal state
		wave = 1;
		lastSpawnTime = 0f;
		spawnedFirstWave = false;
		spawnPointTimer = .8f;
		bridgeFadeValue = .65f;
		score = 0;
		kills = 0;
		updateGuiText (score, kills);
		gameOverPanel.SetActive (false);
		gameOver = false;
		timeSinceSceneLoad = Time.time;
		playerPosition = "central";

		//Initialize bridge objects
		leftBridge.gameObject.SetActive (false);
		leftBridgeActive = false;
		rightBridge.gameObject.SetActive (false);
		rightBridgeActive = false;

		// Initialize player locations
		float centralX = (centralBridge.endingCor [1].x + centralBridge.endingCor [0].x) / 2;
		float centralY = (centralBridge.endingCor [1].y + centralBridge.endingCor [0].y) / 2;
		playerCentralPosition.x = centralX + (Mathf.Sin (centralBridge.transform.eulerAngles.z) * distanceFromBridge);
		playerCentralPosition.y = centralY + (Mathf.Cos (centralBridge.transform.eulerAngles.z) * distanceFromBridge);

		float leftX = (leftBridge.endingCor [1].x + leftBridge.endingCor [0].x) / 2;
		float leftY = (leftBridge.endingCor [1].y + leftBridge.endingCor [0].y) / 2;
		playerLeftPosition.x = leftX + (Mathf.Sin (Mathf.Deg2Rad * (360 - leftBridge.transform.eulerAngles.z)) * distanceFromBridge);
		playerLeftPosition.y = leftY + (Mathf.Cos (Mathf.Deg2Rad * (360 - leftBridge.transform.eulerAngles.z)) * distanceFromBridge);

		float rightX = (rightBridge.endingCor [1].x + rightBridge.endingCor [0].x) / 2;
		float rightY = (rightBridge.endingCor [1].y + rightBridge.endingCor [0].y) / 2;
		playerRightPosition.x = rightX - (Mathf.Sin (Mathf.Deg2Rad * rightBridge.transform.eulerAngles.z) * distanceFromBridge);
		playerRightPosition.y = rightY + (Mathf.Cos (Mathf.Deg2Rad * rightBridge.transform.eulerAngles.z) * distanceFromBridge);

		player.movePlayer (playerCentralPosition);

		//Initialize enemies
		foreach(GameObject spawn in GameObject.FindGameObjectsWithTag ("C spawn")) {
			spawnLocations.Add(spawn);
		}

		// Init the ad to be shown
		Advertisement.Initialize ("60676", false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!gameOver) {
			if (secondBridgeSpawn == wave && !leftBridgeActive) {
				leftBridge.buildBridge ();
				leftBridgeActive = true;
				leftArrow.ChangeInteration(true);
				rightArrow.ChangeInteration(true);
				foreach (GameObject spawn in GameObject.FindGameObjectsWithTag ("L spawn")) {
					spawnLocations.Add (spawn);
				}
			} else if (secondBridgeSpawn == (wave + 1)) {
				leftBridge.beginConstruction (bridgeFadeValue);
			}

			if (thirdBridgeSpawn == wave && !rightBridgeActive) {
				rightBridge.buildBridge ();
				rightBridgeActive = true;
				foreach (GameObject spawn in GameObject.FindGameObjectsWithTag ("R spawn")) {
					spawnLocations.Add (spawn);
				}
			} else if (thirdBridgeSpawn == (wave + 1)) {
				rightBridge.beginConstruction (bridgeFadeValue);
			}

			if (Input.GetMouseButtonDown (0)) {
				if (!leftArrow.pressed && !rightArrow.pressed) {
					player.FireArrow (Camera.main.ScreenToWorldPoint (Input.mousePosition));
				}
			}

			if(Input.GetKeyDown(KeyCode.Escape)) {
				Application.Quit();
			}
	
			spawnEnemiesManager ();
		}
	}

	private void spawnEnemiesManager () {
		var timeDifference = Time.time - timeSinceSceneLoad;
		if (timeDifference > deleyToSpawn) {
			if (!spawnedFirstWave) {
				approachingWave.enableBlinking();
				updateWave();
				lastSpawnTime = timeDifference;
				spawnedFirstWave = true;

				//Spawn the first wave of enemies
				StartCoroutine(spawnWave ());
			} else if (timeDifference > lastSpawnTime + timeBetweenWavesSec) {
				lastSpawnTime = timeDifference;
				wave = wave + 1;
				approachingWave.enableBlinking();
				updateWave();

				// Spawn the nth wave of enemies
				StartCoroutine(spawnWave ());
			}
		}
	}

	IEnumerator spawnWave() {
		List<string> enemyChooser = new List<string> ();
		int numberOfSwordMen = 0, numberOfLightScouts = 0, numberOfSpearmen = 0;

		// Choose how many of each type of enemy to spawn
		if (swordKnightSpawn <= wave) {
			float swordmenDifficulty = Mathf.Log((wave - swordKnightSpawn + 1) * 10, difficultyMultiplier);
			numberOfSwordMen = (int) (swordmenDifficulty * Random.Range (swordmanMinSpawn, swordmanMaxSpawn));
		}
		if (lightScountSpawn <= wave) {
			float lightScoutDifficulty = Mathf.Log((wave - lightScountSpawn + 1) * 10, difficultyMultiplier);
			numberOfLightScouts = (int) (lightScoutDifficulty * Random.Range (lightScoutMinSpawn, lightScoutMaxSpawn));
		}

		float spearmenDifficulty = Mathf.Log(wave * 10, difficultyMultiplier);
		numberOfSpearmen = (int) (spearmenDifficulty * Random.Range (spearmanMinSpawn, spearmanMaxSpawn));

		// Add the enemies to be spawned to a array to be randomly generated
		for (int i= 0; i < numberOfSpearmen; i++) {
			enemyChooser.Add ("spearman");
		}

		for (int i= 0; i < numberOfSwordMen; i++) {
			enemyChooser.Add ("swordman");
		}

		for (int i= 0; i < numberOfLightScouts; i++) {
			enemyChooser.Add ("scout");
		}

		// Generate the enemies
		while (enemyChooser.Count > 0) {
			int index = Random.Range (0, enemyChooser.Count - 1);
			string enemy = enemyChooser[index];
			spawnEnemy (enemy);
			enemyChooser.RemoveAt (index);
			yield return new WaitForSeconds (spawnPointTimer);
		}
	}

	private void spawnEnemy(string type) {
		Enemy enemy;

		if (type == "spearman") {
			GameObject spawn = spawnLocations[Random.Range(0, spawnLocations.Count)];
			enemy = Instantiate (spearmanPrefab, spawn.transform.position, Quaternion.identity) as Enemy;
			enemy.assignedBridge = assignBridge(spawn.tag);
			activeEnemies.Add (enemy);
		}

		if (type == "swordman") {
			GameObject spawn = spawnLocations[Random.Range(0, spawnLocations.Count)];
			enemy = Instantiate (swordmanPrefab, spawn.transform.position, Quaternion.identity) as Enemy;
			enemy.assignedBridge = assignBridge(spawn.tag);
			activeEnemies.Add (enemy);
		}

		if (type == "scout") {
			GameObject spawn = spawnLocations[Random.Range(0, spawnLocations.Count)];
			enemy = Instantiate (lightscoutPrefab, spawn.transform.position, Quaternion.identity) as Enemy;
			enemy.assignedBridge = assignBridge(spawn.tag);
			activeEnemies.Add (enemy);
		}
	}

	private string assignBridge (string tag) {
		string bridge;

		if (tag == "C spawn") {
			bridge = "bridge central";
		} else if (tag == "L spawn") {
			bridge = "bridge left";
		} else {
			bridge = "bridge right";
		}

		return bridge;
	}

	public void updatePoints(int points) {
		score += points;
		kills += 1;
		updateGuiText (score, kills);
	}

	private void updateGuiText(int points, int kills) {
		scoreText.text = "Score: " + points;
		killText.text = "Kills: " + kills;
	}

	private void updateWave() {
		waveText.text = "Wave: " + wave;
	}

	public void GameOver() {
		gameOver = true;
		Time.timeScale = 0;
		leftArrow.ChangeInteration (false);
		rightArrow.ChangeInteration (false);
		gameOverPanel.SetActive (true);

		if (PlayerPrefs.GetInt ("highScore") < score) {
			PlayerPrefs.SetInt ("highScore", score);
		}
		PlayerPrefs.SetInt ("lastScore", score);
		if (PlayerPrefs.GetInt ("topKills") < kills) {
			PlayerPrefs.SetInt ("topKills", kills);
		}
		if (PlayerPrefs.GetInt ("highWave") < wave) {
			PlayerPrefs.SetInt ("highWave", wave);
		}
		PlayerPrefs.SetInt ("attempts", PlayerPrefs.GetInt("attempts", 0) + 1);
		PlayerPrefs.Save ();
	}

	public void loadLevel(string level) {
		Time.timeScale = 1;
		Destroy (this);
		Destroy (instance);
		instance = null;
		StartCoroutine (PlayUnityAd ());
		SceneManager.LoadScene(level);
	}

	public void MovePlayerRight () {
		if (playerPosition == "central" && rightBridgeActive) {
			player.movePlayer (playerRightPosition);
			playerPosition = "right";
		} else if (playerPosition == "left") {
			player.movePlayer (playerCentralPosition);
			playerPosition = "central";
		}
	}

	public void MovePlayerLeft () {
		if (playerPosition == "central") {
			player.movePlayer (playerLeftPosition);
			playerPosition = "left";
		} else if (playerPosition == "right") {
			player.movePlayer (playerCentralPosition);
			playerPosition = "central";
		}
	}

	private IEnumerator PlayUnityAd () {
		while (!Advertisement.IsReady ()) {
			yield return null;
		}
		Advertisement.Show ();
	}
}