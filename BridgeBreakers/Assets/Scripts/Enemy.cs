﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int hp;
	public string assignedBridge;
	public float enemySpeed;
	public int points;

	private GameObject castle;
	private GameObject castleFront;
	private Vector3 bridgeBeginning;
	private Vector3 bridgeEnding;

	private bool hasStartedBridge;
	private bool hasEndedBridge;
	private bool hasReachedCastleFrontEntrance;
	private GameManager gm;

	private Rigidbody2D rb;
	private SpriteRenderer rend;
	private Color originalColor;
	private int bridgeColumn;
	void Start () {
		hasStartedBridge = false;
		hasEndedBridge = false;
		hasReachedCastleFrontEntrance = false;

		gm = GameManager.instance;
		rb = GetComponent<Rigidbody2D> ();
		rend = GetComponent<SpriteRenderer> ();

		originalColor = rend.color;
		castle = GameObject.FindGameObjectWithTag("castle");
		castleFront = GameObject.FindGameObjectWithTag ("castle front");
		GameObject bridgeObj = GameObject.FindGameObjectWithTag (assignedBridge);

		if (bridgeObj != null) {
			Bridge bridge = bridgeObj.GetComponent<Bridge>();
			bridgeColumn = Random.Range (0, bridge.begginingCor.Count);
			bridgeBeginning = bridge.begginingCor[bridgeColumn];
			bridgeEnding = bridge.endingCor[bridgeColumn];
		}

	}

	// Update is called once per frame
	void Update () {
	 	if (!GameManager.gameOver) {
			CheckIfBridgeReached ();
			// Update the movement of the character
			MoveEnemyTowardsCastle ();
		}
	}

	void CheckIfBridgeReached() {

		if ((Mathf.Abs(transform.position.x  - bridgeBeginning.x) < .1) && (Mathf.Abs(transform.position.y  - bridgeBeginning.y) < .1)) {
			hasStartedBridge = true;
		}

		if ((Mathf.Abs(transform.position.x  - bridgeEnding.x) < .1) && (Mathf.Abs(transform.position.y  - bridgeEnding.y) < .1)) {
			hasEndedBridge = true;
		}

		if ((Mathf.Abs(transform.position.x  - castleFront.transform.position.x) < .1) && (Mathf.Abs(transform.position.y  - castleFront.transform.position.y) < .1)) {
			hasReachedCastleFrontEntrance = true;
		}
	}

	void OnTriggerEnter2D(Collider2D  collider) 
	{

		if (collider.tag == "arrow") {
			Destroy (collider.gameObject);
			StartCoroutine (enemyHit ());
		} else if (collider.tag == "castle") {
			GameManager.instance.GameOver();
		}
	}

	IEnumerator enemyHit() {
		rend.color = Color.red;
		yield return new WaitForSeconds(.1f);
		decrementEnemyHp(1);
		rend.color = originalColor;
	}

	private void decrementEnemyHp(int healthLoss) {
		hp -= healthLoss;

		if (hp <= 0) {
			Destroy (this.gameObject);
			gm.updatePoints(points);
		}
	}

	private void MoveEnemyTowardsCastle () {
		Vector3 enemyDir = new Vector3();

		if (hasStartedBridge == false) {
			enemyDir = bridgeBeginning - transform.position;
		} else if (hasEndedBridge == false) {
			enemyDir = bridgeEnding - transform.position;
		} else if (hasReachedCastleFrontEntrance == false){
			enemyDir = castleFront.transform.position - transform.position;
		} else {
			enemyDir = castle.transform.position - transform.position;
		}

		enemyDir.Normalize ();
		rb.velocity = enemyDir * enemySpeed;
	}

}
