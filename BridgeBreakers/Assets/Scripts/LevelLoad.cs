﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoad : MonoBehaviour {
	private static bool isMute;
	
	public void loadLevel(string level) {
		SceneManager.LoadScene(level);
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
