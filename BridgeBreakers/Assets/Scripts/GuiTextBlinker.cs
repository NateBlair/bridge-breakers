﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiTextBlinker : MonoBehaviour {

	private Text flashingText;
	public float timeToBlink;
	public int blinkSpeed;
	private float secondsBlinkedFor;

	void Start() {
		secondsBlinkedFor = 0;
		flashingText = gameObject.GetComponentInChildren<Text>();
		flashingText.enabled = false;
		this.gameObject.SetActive (false);
	}

	IEnumerator BlinkText(){
		//blink it forever. You can set a terminating condition depending upon your requirement. Here you can just set the isBlinking flag to false whenever you want the blinking to be stopped.
		while(Time.time < secondsBlinkedFor + timeToBlink){
			flashingText.enabled = true;
			yield return new WaitForSeconds(.5f);
			flashingText.enabled = false;
			yield return new WaitForSeconds(.5f);
		}
		this.gameObject.SetActive (false);
	}

	public void enableBlinking() {
		secondsBlinkedFor = Time.time;
		this.gameObject.SetActive (true);
		StartCoroutine (BlinkText ());
	}
}
