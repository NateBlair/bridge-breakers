﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	private GameObject bow;
	public int arrowSpeed;
	public GameObject arrowPrefab; 
	public LevelLoad leftArrow;
	// Use this for initialization

	void Start() {
		bow = GameObject.FindGameObjectWithTag("bow");
	}

	public void FireArrow(Vector2 mousePosition) {
		Vector2 arrowDir;
		arrowDir.x = mousePosition.x - bow.transform.position.x;
		arrowDir.y = mousePosition.y - bow.transform.position.y;
		arrowDir.Normalize ();
		float rot_z = Mathf.Atan2 (arrowDir.y, arrowDir.x) * Mathf.Rad2Deg;
		GameObject arrowClone = Instantiate (arrowPrefab, bow.transform.position, Quaternion.Euler(0f, 0f, rot_z)) as GameObject;
		Rigidbody2D rb = arrowClone.GetComponent<Rigidbody2D>();
		rb.velocity = arrowDir * arrowSpeed;
		Destroy (arrowClone, 5);
	}

	public void movePlayer(Vector3 newPosition) {
		transform.position = newPosition;
	}
}
