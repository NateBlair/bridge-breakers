﻿using UnityEngine;
using System.Collections;

public class CameraScale : MonoBehaviour {

	public float normalCamSize;
	public float normalScreenRes;

	void Start () {
		float estimatedNewCamSize = (normalCamSize * normalScreenRes) / Screen.width;
		if (estimatedNewCamSize > normalCamSize) {
			Camera.main.orthographicSize = estimatedNewCamSize;
		}
	}
}
